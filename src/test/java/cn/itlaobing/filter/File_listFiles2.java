package cn.itlaobing.filter;

import java.io.File;
import java.io.FileFilter;

public class File_listFiles2 {
	public static void main(String[] args) {
		File dir=new File(".");//.表示当前目录
		FileFilter filter=new FileFilter() {
			//匿名内部类
			public boolean accept(File file) {
				String name=file.getName();
				System.out.println("正在过滤"+name);
				return name.startsWith(".");
			}
	};
	File[] subs=dir.listFiles(filter);
	for(int i=0;i<subs.length;i++) {
		File sub=subs[i];
		System.out.println(sub.getName());
	}
	}
}
