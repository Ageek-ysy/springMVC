package cn.itlaobing.ssm;

import cn.itlaobing.springmvc.entity.Student;
import cn.itlaobing.springmvc.service.StudentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@ContextConfiguration("classpath:applicationContext-root.xml")
public class TestService {

    @Autowired
    private StudentService studentService;


    @Test
    public void test(){
        List<Student> list = studentService.findAll();
        System.out.println(list);
    }

}
