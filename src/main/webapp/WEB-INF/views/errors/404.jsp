<%--
  Created by IntelliJ IDEA.
  User: ageek
  Date: 17-12-11
  Time: 下午2:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>资源请求错误</title>
    <link rel="stylesheet" type="text/css" href="/assets/app/css/style.css">
</head>
<body>
<div class="wavetext">
    <div class="wavetop">
        <div class="wavelx">
            <img src="/assets/app/imgs/404.svg">
        </div>
        <div class="wavets">
            em~ 此路不通！
        </div>
    </div>
    <span class="wavecz">你可以回到网站首页或关闭本页~</span>
    <div class="waveqcz">
    </div>
    <div class="wavegohome waveinput">
        <a href="http://www.jsdaima.com">回到首页</a>
    </div>
    <div class="waveclose waveinput">
        <a href="javascript:void(0);" onclick="window.opener=null; window.open('','_self');window.close();">关闭页面</a>
    </div>
    <div class="waveqcz">
    </div>
    <div class="waveqczfd">
    </div>
</div>
<div class="waveWrapper waveAnimation">
    <div class="waveWrapperInner bgTop">
        <div class="wave waveTop" style="background-image: url('/assets/app/imgs/wave-top.png')">
        </div>
    </div>
    <div class="waveWrapperInner bgMiddle">
        <div class="wave waveMiddle" style="background-image: url('/assets/app/imgs/wave-mid.png')">
        </div>
    </div>
    <div class="waveWrapperInner bgBottom">
        <div class="wave waveBottom" style="background-image: url('/assets/app/imgs/wave-bot.png')">
        </div>
    </div>
</div>
</body>
</html>
