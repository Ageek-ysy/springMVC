<%@ page import="java.util.Enumeration" %><%--
  Created by IntelliJ IDEA.
  User: ageek
  Date: 17-12-4
  Time: 上午11:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>第一个springMVC程序</title>
</head>
<body>

<a href="${pageContext.request.contextPath}/students">点击进入学生管理系统</a>


${msg}

<br/>
<h3>request范围中的值</h3>
<%
    Enumeration<String> requestNames = request.getAttributeNames();
    while (requestNames.hasMoreElements()){
        String name = requestNames.nextElement();
        Object value = request.getAttribute(name);
        out.print("<div>"+name+">>>>>>>>>>>>>"+value.getClass().getName()+"</div>");
    }
%>

<br>
<h3>session范围中的值</h3>
<%
    Enumeration<String> sessionNames = session.getAttributeNames();
    while (sessionNames.hasMoreElements()){
        String name = sessionNames.nextElement();
        Object value = session.getAttribute(name);
        out.print("<div>"+name+">>>>>>>>>>>>>"+value.getClass().getName()+"</div>");
    }
%>
<br>
<h3>application范围中的值</h3>
<%
    Enumeration<String> applicationNames = application.getAttributeNames();
    while (applicationNames.hasMoreElements()){
        String name = applicationNames.nextElement();
        Object value = application.getAttribute(name);
        out.print("<div>"+name+">>>>>>>>>>>>>"+value.getClass().getName()+"</div>");
    }
%>

</body>
</html>
