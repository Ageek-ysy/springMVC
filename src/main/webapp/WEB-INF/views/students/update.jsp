<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/12/5 0005
  Time: 下午 4:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page pageEncoding="UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>更新学生</title>
    <link type="text/css" rel="stylesheet" href="${ctx}/assets/lib/css/bootstrap-theme.min.css">
    <link type="text/css" rel="stylesheet" href="${ctx}/assets/lib/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="${ctx}/assets/lib/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="${ctx}/assets/lib/css/jquery.toast.css">
    <link type="text/css" rel="stylesheet" href="${ctx}/assets/lib/css/bootstrap-datepicker.min.css">
</head>
<body>
<div class="container">
    <div style="margin-top: 30px"></div>
<form:form cssClass="form-horizontal" modelAttribute="student">
    <input type="hidden" name="_method" value="PUT">
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">姓名</label>
        <div class="col-sm-4">
            <form:input path="name" cssClass="form-control" value="${student.name}"/>
        </div>
    </div>

    <div class="form-group">
        <label for="gender" class="col-sm-2 control-label">性别</label>
        <div class="col-sm-4">
            <form:radiobuttons path="gender" items="${gender}" delimiter="&nbsp;&nbsp;&nbsp;&nbsp;" itemLabel="text"/>
        </div>
    </div>


    <div class="form-group">
        <label for="birthday" class="col-sm-2 control-label">生日</label>
        <div class="col-sm-4">
            <form:input path="birthday" cssClass="form-control choose_date"/>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success">更新</button>
        </div>
    </div>
</form:form>
</div>
<script type="application/javascript" src="${ctx}/assets/lib/js/jquery.min.js"></script>
<script type="application/javascript" src="${ctx}/assets/lib/js/bootstrap.min.js"></script>
<script type="application/javascript" src="${ctx}/assets/lib/js/bootstrap-datepicker.min.js"></script>
<script type="application/javascript" src="${ctx}/assets/lib/js/bootstrap-datepicker.zh-CN.min.js"></script>
<script type="application/javascript" src="${ctx}/assets/app/js/student.js"></script>
</body>
</html>
