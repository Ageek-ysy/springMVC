<%--
  Created by IntelliJ IDEA.
  User: ageek
  Date: 17-12-5
  Time: 下午9:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>学生列表</title>
    <link type="text/css" rel="stylesheet" href="${ctx}/assets/lib/css/bootstrap-theme.min.css">
    <link type="text/css" rel="stylesheet" href="${ctx}/assets/lib/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="${ctx}/assets/lib/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="${ctx}/assets/lib/css/jquery.toast.css">
</head>
<body>
<div class="container" style="margin-top: 50px">
    <a class="btn btn-danger pull-right" href="${ctx}/students/create">
        <i class="fa fa-plus-circle"></i>添加学生
    </a>
    <div class="clearfix"></div>
    <hr>
    <table class="table table-bordered table-hover table-condensed table-striped" border="1" cellpadding="0" cellspacing="0" width="800">
        <tr>
            <th>编号</th>
            <th>姓名</th>
            <th>性别</th>
            <th>生日</th>
            <th>操作</th>
        </tr>

        <c:forEach var="student" items="${students}">
            <tr align="center">
                <td>${student.id}</td>
                <td>${student.name}</td>
                <td>${student.gender.text}</td>
                <td>
                    <fmt:formatDate value="${student.birthday}" pattern="yyyy-MM-dd"/>
                </td>
                <td>
                    <a class="btn btn-warning" href="${ctx}/students/${student.id}/update"><i class="fa fa-pencil-square-o"></i></a>
                    <a class="btn btn_delete btn-danger" href="${ctx}/students/${student.id}"><i class="fa fa-trash-o"></i></a>
                </td>
            </tr>
        </c:forEach>
    </table>

</div>
<script type="application/javascript" src="${ctx}/assets/lib/js/jquery.min.js"></script>
<script type="application/javascript" src="${ctx}/assets/lib/js/bootstrap.min.js"></script>
<script type="application/javascript" src="${ctx}/assets/lib/js/jquery.toast.js"></script>
<script type="application/javascript" src="${ctx}/assets/app/js/student.js"></script>

<script>
    $(function () {
        <c:if test="${not empty message}">
            $.toast({
                heading: '提示',
                text: '${message}',
                showHideTransition: 'slide',
                icon: 'success',
                position: 'top-right'
            })
        </c:if>
    });

</script>
</body>
</html>
