<%--
  Created by IntelliJ IDEA.
  User: ageek
  Date: 17-12-5
  Time: 下午10:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>添加学生</title>
    <link type="text/css" rel="stylesheet" href="${ctx}/assets/lib/css/bootstrap-theme.min.css">
    <link type="text/css" rel="stylesheet" href="${ctx}/assets/lib/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="${ctx}/assets/lib/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="${ctx}/assets/lib/css/jquery.toast.css">
    <link type="text/css" rel="stylesheet" href="${ctx}/assets/lib/css/bootstrap-datepicker.min.css">

</head>
<body>

<div class="container">
    <div style="margin-top: 30px"></div>
    <form:form cssClass="form-horizontal" modelAttribute="student">
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">姓名</label>
            <div class="col-sm-4">
                <form:input path="name" cssClass="form-control" placeholder="请输入姓名"/>
            </div>
            <div class="col-sm-6">
                <form:errors path="name" cssClass="text-danger"></form:errors>
            </div>
        </div>

        <div class="form-group">
            <label for="gender" class="col-sm-2 control-label">性别</label>
            <div class="col-sm-4">
                <form:radiobuttons path="gender" items="${gender}" itemLabel="text" delimiter="&nbsp;&nbsp;&nbsp;&nbsp;"/>
            </div>
        </div>


        <div class="form-group">
            <label for="birthday" class="col-sm-2 control-label">生日</label>
            <div class="col-sm-4">
                <form:input path="birthday" cssClass="form-control choose_date" placeholder="请选择日期"/>
            </div>
            <div class="col-sm-4">
                <form:errors path="birthday" cssClass="text-danger"/>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-success">添加</button>
            </div>
        </div>
        </form:form>
</div>

<script type="application/javascript" src="${ctx}/assets/lib/js/jquery.min.js"></script>
<script type="application/javascript" src="${ctx}/assets/lib/js/bootstrap.min.js"></script>
<script type="application/javascript" src="${ctx}/assets/lib/js/bootstrap-datepicker.min.js"></script>
<script type="application/javascript" src="${ctx}/assets/lib/js/bootstrap-datepicker.zh-CN.min.js"></script>
<script type="application/javascript" src="${ctx}/assets/app/js/student.js"></script>
</body>
</html>
