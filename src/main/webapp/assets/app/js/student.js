$(document).on("focus",".choose_date",function () {
    $(this).datepicker({
        autoclose:true,
        format:'yyyy-mm-dd',
        startView:'month',
        todayHighlight: true,
        language: 'zh-CN',
        viewSelect: 'day'
    });
});



$(function () {
    $(".btn_delete").click(function (e) {
        e.stopPropagation();
        e.preventDefault();
        var $a=$(this);
        var href=$(this).attr("href");
        $.post(href,{_method:'delete'},function (data) {
            if (data && data.status=='SUCCESS'){
                $.toast({
                    heading:'提示',
                    text:data.message,
                    showHideTransition:'slide',
                    icon:'success',
                    position:'top-right'
                });
                $a.parent().parent().remove();
                // alert("删除成功!");
                // window.location.reload(true);
            }else{
                $.toast({
                    heading:'提示',
                    text:"删除失败",
                    showHideTransition:'slide',
                    icon:'error',
                    position:'top-right'
                });
                // alert("删除失败！")
            }
        });
    });
});

