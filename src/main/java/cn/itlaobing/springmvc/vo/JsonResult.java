package cn.itlaobing.springmvc.vo;

import java.io.Serializable;

public class JsonResult<T> implements Serializable {
    private Status status=Status.INFO;//返回状态
    private String message="";//返回消息
    private T content;//返回内容

    public JsonResult(Status status, String message, T content) {
        this.status = status;
        this.message = message;
        this.content = content;
    }

    public JsonResult(Status status, String message) {
        this.status = status;
        this.message = message;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }
}
