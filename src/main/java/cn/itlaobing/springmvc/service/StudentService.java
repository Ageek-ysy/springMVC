package cn.itlaobing.springmvc.service;

import cn.itlaobing.springmvc.entity.Student;

import java.util.List;

public interface StudentService {
    public List<Student> findAll();
    public Student findById(Long id);
    public Student saveOrUpdate(Student student);
    public void delete(Long id);
}
