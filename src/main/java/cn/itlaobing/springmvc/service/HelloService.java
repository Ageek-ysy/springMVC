package cn.itlaobing.springmvc.service;

public interface HelloService {
    public String sayHello(String name);
}
