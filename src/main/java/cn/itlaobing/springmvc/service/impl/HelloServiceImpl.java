package cn.itlaobing.springmvc.service.impl;

import cn.itlaobing.springmvc.service.HelloService;
import org.springframework.stereotype.Service;

@Service
public class HelloServiceImpl implements HelloService {
    @Override
    public String sayHello(String name) {
        return "你好"+name;
    }
}
