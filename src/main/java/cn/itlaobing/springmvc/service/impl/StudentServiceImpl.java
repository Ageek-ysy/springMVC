package cn.itlaobing.springmvc.service.impl;

import cn.itlaobing.springmvc.entity.Student;
import cn.itlaobing.springmvc.mapper.StudentMapper;
import cn.itlaobing.springmvc.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentMapper studentMapper;

    @Override
    public List<Student> findAll() {
        List<Student> list = studentMapper.findAll();
        return list;
    }

    @Override
    public Student findById(Long id) {
        Student student = studentMapper.selectByPrimaryKey(id);
        return student;
    }

    public Student saveOrUpdate(Student student) {
        if (student.getId()!=null){
            Student se = findById(student.getId());
            if (se!=null){
                studentMapper.updateByPrimaryKeySelective(student);
            }
        }else{
            studentMapper.insert(student);
        }
        return student;
    }

    @Override
    public void delete(Long id) {
        studentMapper.deleteByPrimaryKey(id);
    }
}
