package cn.itlaobing.springmvc.web.controller;

import cn.itlaobing.springmvc.entity.Gender;
import cn.itlaobing.springmvc.entity.Student;
import cn.itlaobing.springmvc.service.StudentService;
import cn.itlaobing.springmvc.vo.JsonResult;
import cn.itlaobing.springmvc.vo.Status;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping(path = "/students")
public class StudentController {

    private Logger logger = Logger.getLogger(this.getClass());

    @Autowired
    private StudentService studentService;

    @GetMapping
    public String findAll(Model model){
        model.addAttribute("students",studentService.findAll());
        String value = null;
        System.out.println(value.charAt(0));
        return "/students/index";
    }

    @GetMapping(path = "/create")
    public String create(){
        //转向create.jsp页面
        return "/students/create";
    }

    @PostMapping(path = "/create")
    public String create(@Valid @ModelAttribute Student student,
                         BindingResult bindingResult,
                         RedirectAttributes redirectAttributes){
        //执行处理器方法前，首先要进行数据验证，会有一个结果bindingResult
        if (bindingResult.hasErrors()){//如果有错误，则应该返回表单视图
            //因为表单要与student进行绑定，所以他必须放到Model中
            return "/students/create";
        }
        //实现添加功能
        studentService.saveOrUpdate(student);
        redirectAttributes.addFlashAttribute("message","添加成功");
        return "redirect:/students";
    }


    @GetMapping(path = "/{id}/update")
    public String update(@PathVariable Long id,Model model){
        //转向update.jsp页面
        return "/students/update";
    }

    @PutMapping(path = "/{id}/update")
    public String update(@ModelAttribute("student") Student student,RedirectAttributes redirectAttributes){
        //实现更新功能
        studentService.saveOrUpdate(student);
        redirectAttributes.addFlashAttribute("message","修改成功");
        return "redirect:/students";
    }

    @DeleteMapping(path = "/{id}")
    @ResponseBody
    public JsonResult<String> delete(@PathVariable Long id){
        studentService.delete(id);
        JsonResult<String> result = new JsonResult<String>(Status.SUCCESS,"删除成功");
        return result;
    }

    @ModelAttribute
    public void prepareDate(HttpServletRequest request,Model model,@PathVariable(required = false) Long id){
        //@ModelAttribute  加载controller时先加载被这个标识的方法，所以先将页面需要的东西提前存到Model中
        model.addAttribute("gender",Gender.values());
        if (id!=null){
            //如果请求路径有id，则根据id把对象查出来，存放到Model中
            model.addAttribute("student",studentService.findById(id));
        }else{
            model.addAttribute("student",new Student());
        }
    }

}
