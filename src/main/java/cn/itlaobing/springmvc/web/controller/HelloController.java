package cn.itlaobing.springmvc.web.controller;

import cn.itlaobing.springmvc.service.HelloService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.util.Map;

@Controller
public class HelloController {

    private Logger logger = Logger.getLogger(this.getClass());

    @Autowired
    private RequestMappingHandlerMapping handlerMapping;


    @Autowired
    private HelloService helloService;
    /**
     *  @Controller 表示这个类是一个Controller，需要被spring容器托管
     *  @RequestMapping 表示 如果请求的地址是 “/hello”,并且是以GET请求提交的，
     *  就调用 hello方法
     *  调用hello方法的时候，SpringMVC框架看到 有一个参数 Model,此时就会实例化一个Model对象，调用的时候传入。
     *  Model在SpringMVC中用来存放数据，这些数据会被带入到视图上，视图上使用EL取出数据进行渲染。
     * (这些数据被存放到了Request范围中)
     *  返回一个”hello” 这个hello就是逻辑视图的名字，对应的是JSP，具体JSP存放的路径需要在 spring的配置问价中进行配置
     * @param model
     * @return
     */
    @RequestMapping(value = "/hello",method = RequestMethod.GET)
    public String hello(String name,Model model){
        //handlerMapping中保存的是请求与处理器方法之间的映射
        //结论：HandlerMapping是一个对象，这个对象的创建是在容器启动后创建的。
        // 容器启动后，会扫描所有的Controller类 中的 @RequestMapping,
        // 然后将 请求的地址信息和对应的方法放到 一个Map中，
        // 即Map中的key是请求的信息（url+method,它被Spring框架内部封装成了RequestMappingInfo）, value
        //是一个方法Controller类中的方法对象
        // (反射中的Method,被Spring框架内部封装成了HandlerMethod对象)。
        //当请求到达了DispatcherServlet 之后，
        // DispatcherServlet首先到Spring容器中获取HandlerMapping对象，
        // 然后对比请求的地址信息与HandlerMapping对象中存储的RequestMappingInfo是否一致，
        // 如果一致，则取出HandlerMethod对象，然后调用这个方法，如果没有找到，则出现404错误
        Map<RequestMappingInfo,HandlerMethod> map = handlerMapping.getHandlerMethods();
        for(Map.Entry<RequestMappingInfo,HandlerMethod> entry:map.entrySet()){
            RequestMappingInfo requestMappingInfo = entry.getKey();
            HandlerMethod handlerMethod = entry.getValue();
            logger.info(requestMappingInfo.toString()+">>>>>>>>>>>>>>>>>>>>"+handlerMethod.getShortLogMessage());
        }
        model.addAttribute("msg",helloService.sayHello(name));
        return "hello";
    }
}
