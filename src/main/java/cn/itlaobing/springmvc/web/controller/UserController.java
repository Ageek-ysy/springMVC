package cn.itlaobing.springmvc.web.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/users")
public class UserController {

    private Logger logger = Logger.getLogger(this.getClass());
    //@RequestMapping所有的参数都是数组类型的

    @GetMapping
    public String index(){
        logger.info(">>>index>>>>>>>>>GET>>>>>>>>>>>");
        //TODO  查询所有用户
        return "";
    }

    @PostMapping
    public String create(){
        logger.info(">>>>>>create>>>>>>>>>POST>>>>>>>>>>>>");
        //TODO  新增用户
        return "";
    }

    @GetMapping(path = "/{id}")
    //{id}叫做路径参数PathVariable  ,在处理器方法上使用@PathVariable来获取到
    //路径参数的值，如果路径参数的名称和处理器方法参数的名称不一致，
    // 就需要在@PathVariable指明
    //如果一致，则不需要指定路径参数的名称
    public String show(@PathVariable Long id){
        logger.info(">>>>>>>show>>>>>>>>GET>>>>.");
        //TODO  显示详情
        return "";
    }

    @PutMapping(path = "/{id}")
    public String update(@PathVariable Long id){
        logger.info(">>>>>>>>update>>>>PUT>>>>");
        //TODO  更新用户
        return "";
    }

    @DeleteMapping(path = "/{id}")
    public String destroy(@PathVariable Long id){
        logger.info(">>>>>destroy>>>>>>>>>>DELETE>>>>>>>>>.");
        //TODO  销毁用户
        return "";
    }

}