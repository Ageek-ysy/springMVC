package cn.itlaobing.springmvc.web.advice;

import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice(basePackages = "cn.itlaobing.springmvc.web.controller")
public class GlobalExceptionAdvice {

    private Logger logger = Logger.getLogger(this.getClass());

    /**
     * 所有异常都交由这个方法处理
     * @param model
     * @param e
     * @return
     */
    @ExceptionHandler(Exception.class)
    public String handleException(Model model,Exception e){
        logger.info(e);
        model.addAttribute("error",e.toString());
        return "/errors/500";
    }

    /**
     *cn.itlaobing.springmvc.web.controller 包下所有方法执行之前，先执行这个方法
     * 这个方法将上下文存放到Model中，渲染视图的时候，带到视图上去
     * @param request
     * @param model
     */
    public void context(HttpServletRequest request, Model model){
        logger.info(request.getContextPath());
        model.addAttribute("ctx",request.getContextPath());
    }
}
