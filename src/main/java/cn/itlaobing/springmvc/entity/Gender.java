package cn.itlaobing.springmvc.entity;

public enum Gender {
    MALE("男"),
    FEMALE("女");

    private String text;

    private Gender(String text){
        this.text=text;
    }

    public String getText(){
        return text;
    }

}
