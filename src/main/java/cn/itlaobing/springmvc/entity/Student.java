package cn.itlaobing.springmvc.entity;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

public class Student implements Serializable {
    private Long id;

    @NotBlank(message = "姓名不能为空")
    @Length(min = 3,max = 16,message = "名字范围必须在6~16之间")
    private String name;

    @NotNull(message = "性别不能为空")
    private Gender gender=Gender.MALE;

    @NotNull(message = "生日不能为空")
    @DateTimeFormat(pattern = "yyyy-mm-dd")
    private Date birthday;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Date getBirthday() {
        return birthday;
    }
    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", gender=" + gender +
                ", birthday=" + birthday +
                '}';
    }
}